login_mouse_click(){
    WinGetTitle, Title, A
    WinGet, OutputVar_id, ID , A
    WinGet, OutputVar_pid, PID , A
    WinGet, OutputVar_exe, ProcessName , A

    if(OutputVar_exe == "RuneLite.exe"){

        ; square_top_x_coordinate := 605
        ; square_top_y_coordinate :=453
        ; square_bottom_x_coordinate := 796
        ; square_bottom_y_coordinate := 494
        square_top_x_coordinate := 407
        square_top_y_coordinate := 306
        square_bottom_x_coordinate := 529
        square_bottom_y_coordinate := 329
        delay_min := 300
        delay_max := 500
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max,1)
        ; square_top_x_coordinate := 475
        ; square_top_y_coordinate := 403
        ; square_bottom_x_coordinate := 733
        ; square_bottom_y_coordinate := 417
        square_top_x_coordinate := 318
        square_top_y_coordinate := 269
        square_bottom_x_coordinate := 465
        square_bottom_y_coordinate := 276
        delay_min := 300
        delay_max := 500
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max,1)
        sleep 50
        loop 40 {
            ControlSend , , {BackSpace}, ahk_id %OutputVar_id%,

        }
        ControlSend , , %username%, ahk_id %OutputVar_id%,
        square_top_x_coordinate := 351
        square_top_y_coordinate := 284
        square_bottom_x_coordinate := 493
        square_bottom_y_coordinate := 293
        ; square_top_x_coordinate := 525
        ; square_top_y_coordinate := 434
        ; square_bottom_x_coordinate := 719
        ; square_bottom_y_coordinate := 442
        delay_min := 300
        delay_max := 500
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max,1)
        sleep 50
        loop 40 {
            ControlSend , , {BackSpace}, ahk_id %OutputVar_id%,

        }
        ControlSend , , %passwordd%, ahk_id %OutputVar_id%,
        square_top_x_coordinate := 247
        square_top_y_coordinate := 339
        square_bottom_x_coordinate := 363
        square_bottom_y_coordinate := 360
        ; square_top_x_coordinate := 371
        ; square_top_y_coordinate := 509
        ; square_bottom_x_coordinate := 548
        ; square_bottom_y_coordinate := 535
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max,1)

    }
    else{
        MsgBox % "RuneLite is not activated. Please click on RuneLite window." 
    }
    stop_script := False
}
select_world_mouse_click(){
    WinGetTitle, Title, A
    WinGet, OutputVar_id, ID , A
    WinGet, OutputVar_pid, PID , A
    WinGet, OutputVar_exe, ProcessName , A

    if(OutputVar_exe == "RuneLite.exe"){
        delay_min := 300
        delay_max := 500

        ; square_top_x_coordinate := 20
        ; square_top_y_coordinate := 742
        ; square_bottom_x_coordinate := 144
        ; square_bottom_y_coordinate := 775
        square_top_x_coordinate := 16
        square_top_y_coordinate := 494
        square_bottom_x_coordinate := 100
        square_bottom_y_coordinate := 521
        ; overlay(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate)
        ;    MsgBox % "tu je overlaz: " 
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

        ; square_top_x_coordinate := 947
        ; square_top_y_coordinate := 50
        ; square_bottom_x_coordinate := 959
        ; square_bottom_y_coordinate := 62
        square_top_x_coordinate := 632
        square_top_y_coordinate := 34
        square_bottom_x_coordinate := 638
        square_bottom_y_coordinate := 41
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

        delay_min := 500
        delay_max := 600
        ; square_top_x_coordinate := 107
        ; square_top_y_coordinate := 99
        ; square_bottom_x_coordinate := 199
        ; square_bottom_y_coordinate := 108
        square_top_x_coordinate := 71
        square_top_y_coordinate := 66
        square_bottom_x_coordinate := 143
        square_bottom_y_coordinate := 78
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

    }
    else{
        MsgBox % "RuneLite is not activated. Please click on RuneLite window." 
    }
    stop_script := False
}
logout_mouse_click(){
    WinGetTitle, Title, A
    WinGet, OutputVar_id, ID , A
    WinGet, OutputVar_pid, PID , A
    WinGet, OutputVar_exe, ProcessName , A

    if(OutputVar_exe == "RuneLite.exe"){
        delay_min := 300
        delay_max := 500

        ; square_top_x_coordinate := 1113
        ; square_top_y_coordinate := 44
        ; square_bottom_x_coordinate := 1145
        ; square_bottom_y_coordinate := 71
        square_top_x_coordinate := 634
        square_top_y_coordinate := 498
        square_bottom_x_coordinate := 655
        square_bottom_y_coordinate := 525
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

        ; square_top_x_coordinate := 907
        ; square_top_y_coordinate := 611
        ; square_bottom_x_coordinate := 1096
        ; square_bottom_y_coordinate := 637
        square_top_x_coordinate := 582
        square_top_y_coordinate := 447
        square_bottom_x_coordinate := 709
        square_bottom_y_coordinate := 468
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

    }
    else{
        MsgBox % "RuneLite is not activated. Please click on RuneLite window." 
    }
    stop_script := False
}
exit_runscape_mouse_click(){
    WinGetTitle, Title, A
    WinGet, OutputVar_id, ID , A
    WinGet, OutputVar_pid, PID , A
    WinGet, OutputVar_exe, ProcessName , A

    if(OutputVar_exe == "RuneLite.exe"){
        delay_min := 300
        delay_max := 500

        square_top_x_coordinate := 1181
        square_top_y_coordinate := 13
        square_bottom_x_coordinate := 1199
        square_bottom_y_coordinate := 26
        mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
    }
    else{
        MsgBox % "RuneLite is not activated. Please click on RuneLite window." 
    }
    stop_script := False
}
mine_mouse_click(){
    WinGetTitle, Title, A
    WinGet, OutputVar_id, ID , A
    WinGet, OutputVar_pid, PID , A
    WinGet, OutputVar_exe, ProcessName , A

    if(OutputVar_exe == "RuneLite.exe"){
        ; delay_min := 3700
        ; delay_max := 4000
        delay_min := 6000
        delay_max := 6300
        image_dest := A_ScriptDir . "\images\mining_ore.png"
        detected_coordinates := detect_image_on_active_window(image_dest)
        detected_coordinates := detect_image_on_active_window(image_dest)
        rez := True
        for index, value in detected_coordinates {
            ; MsgBox % index ": value: " value
            if (value == 0) {
                rez := False
            }

        }
        if (rez){
            ; square_top_x_coordinate := 556
            ; square_top_y_coordinate := 474
            ; square_bottom_x_coordinate := 620
            ; square_bottom_y_coordinate := 530
            ; mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            mouse_click(detected_coordinates[1],detected_coordinates[2],detected_coordinates[3],detected_coordinates[4],delay_min,delay_max)
        }
        else {
            MsgBox % "tu smo: " 
        }
        overlay()
    }
    else{
        MsgBox % "RuneLite is not activated. Please click on RuneLite window." 
    }
    ; stop_script := False
}
pizza_tomato(){
    WinGetTitle, Title, A
    WinGet, OutputVar_id, ID , A
    WinGet, OutputVar_pid, PID , A
    WinGet, OutputVar_exe, ProcessName , A
    delay_min := 300
    delay_max := 500
    if(OutputVar_exe == "RuneLite.exe"){
        repeat_number := 10
        loop %repeat_number% {

            square_top_x_coordinate := 286
            square_top_y_coordinate := 182
            square_bottom_x_coordinate := 300
            square_bottom_y_coordinate := 199

            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            random sleep_interval , 1100, 1300
            sleep sleep_interval

            square_top_x_coordinate := 438
            square_top_y_coordinate := 329
            square_bottom_x_coordinate := 456
            square_bottom_y_coordinate := 349
            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

            pizza_image := A_ScriptDir . "\images\dough.png"

            ; x1 := 867
            ; y1 := 289
            ; x2 := 938
            ; y2 := 351

        
x1 := 264
y1 := 110
x2 := 304
y2 := 142
            detected_coordinates := detect_image_on_region(pizza_image,x1,y1,x2,y2)
            rez := True
            for index, value in detected_coordinates {
                ; MsgBox % index ": value: " value
                if (value == 0) {
                    rez := False
                }
            }
            if (rez){
                mouse_click_right(detected_coordinates[1],detected_coordinates[2],detected_coordinates[3],detected_coordinates[4],delay_min,delay_max)
          square_top_x_coordinate := 196
square_top_y_coordinate := 189
square_bottom_x_coordinate := 302
square_bottom_y_coordinate := 193

                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

             square_top_x_coordinate := 369
square_top_y_coordinate := 112
square_bottom_x_coordinate := 390
square_bottom_y_coordinate := 132

                mouse_click_right(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
square_top_x_coordinate := 309
square_top_y_coordinate := 189
square_bottom_x_coordinate := 382
square_bottom_y_coordinate := 191

                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

                square_top_x_coordinate := 486
                square_top_y_coordinate := 45
                square_bottom_x_coordinate := 497
                square_bottom_y_coordinate := 55
                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

                square_top_x_coordinate := 571
                square_top_y_coordinate := 247
                square_bottom_x_coordinate := 593
                square_bottom_y_coordinate := 261
                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

                square_top_x_coordinate := 659
                square_top_y_coordinate := 358
                square_bottom_x_coordinate := 674
                square_bottom_y_coordinate := 367
                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
                square_top_x_coordinate := 216
                square_top_y_coordinate := 421
                square_bottom_x_coordinate := 305
                square_bottom_y_coordinate := 485
                ; MsgBox % "ovdje stane: " 
                random sleep_interval , 500, 700
                sleep sleep_interval
                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
                random sleep_interval , 15000, 20000
                sleep sleep_interval

                ; square_top_x_coordinate := 286
                ; square_top_y_coordinate := 182
                ; square_bottom_x_coordinate := 300
                ; square_bottom_y_coordinate := 199

                ;         mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
                ; random sleep_interval , 1100, 1300
                ; sleep sleep_interval

                ; square_top_x_coordinate := 438
                ; square_top_y_coordinate := 329
                ; square_bottom_x_coordinate := 456
                ; square_bottom_y_coordinate := 349
                ;         mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

                ;         x1 := 5
                ; y1 := 221
                ; x2 := 209
                ; y2 := 244
                ; square_top_x_coordinate := 6
                ; square_top_y_coordinate := 213
                ; square_bottom_x_coordinate := 207
                ; square_bottom_y_coordinate := 249
                ;         image := A_ScriptDir . "\images\14_incomplete_pizzas.png"
                ;         detected_coordinates := detect_image_on_region(image,square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate)

            }
        }

    }
    else{
        MsgBox % "RuneLite is not activated. Please click on RuneLite window." 
    }
    stop_script := False
    MsgBox % "skripta gotova." 

}
pizza_delivery(){

    WinGetTitle, Title, A
    WinGet, OutputVar_id, ID , A
    WinGet, OutputVar_pid, PID , A
    WinGet, OutputVar_exe, ProcessName , A
    delay_min := 300
    delay_max := 500
    if(OutputVar_exe == "RuneLite.exe"){
        repeat_number := 1
        loop %repeat_number% {
            square_top_x_coordinate := 637
            square_top_y_coordinate := 149
            square_bottom_x_coordinate := 639
            square_bottom_y_coordinate := 151

            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            random sleep_interval , 4000, 4300
            sleep sleep_interval

            square_top_x_coordinate := 596
            square_top_y_coordinate := 98
            square_bottom_x_coordinate := 598
            square_bottom_y_coordinate := 97
            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            random sleep_interval , 5300, 5700
            sleep sleep_interval
            ; mouse_click_right(detected_coordinates[1],detected_coordinates[2],detected_coordinates[3],detected_coordinates[4],delay_min,delay_max)
            ;  klkni na vrata
            square_top_x_coordinate := 34
            square_top_y_coordinate := 226
            square_bottom_x_coordinate := 34
            square_bottom_y_coordinate := 235

            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            random sleep_interval , 5000, 5300
            sleep sleep_interval
            ; druga vrata
            square_top_x_coordinate := 94
            square_top_y_coordinate := 241
            square_bottom_x_coordinate := 108
            square_bottom_y_coordinate := 246

            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            random sleep_interval , 4000, 4300
            sleep sleep_interval
            ; mouse_click_right(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            square_top_x_coordinate := 252
            square_top_y_coordinate := 354
            square_bottom_x_coordinate := 264
            square_bottom_y_coordinate := 361
            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            random sleep_interval , 4000, 4300
            sleep sleep_interval
        }

    }
    else{
        MsgBox % "RuneLite is not activated. Please click on RuneLite window." 
    }
    stop_script := False
    MsgBox % "skripta gotova." 

}

pizza_combo(){
    pizza_withdraw()
    pizza_delivery()
}
pizza_withdraw(){

    WinGetTitle, Title, A
    WinGet, OutputVar_id, ID , A
    WinGet, OutputVar_pid, PID , A
    WinGet, OutputVar_exe, ProcessName , A
    delay_min := 300
    delay_max := 500
    if(OutputVar_exe == "RuneLite.exe"){
        repeat_number := 1
        loop %repeat_number% {

            square_top_x_coordinate := 286
            square_top_y_coordinate := 182
            square_bottom_x_coordinate := 300
            square_bottom_y_coordinate := 199

            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            random sleep_interval , 1100, 1300
            sleep sleep_interval

            square_top_x_coordinate := 438
            square_top_y_coordinate := 329
            square_bottom_x_coordinate := 456
            square_bottom_y_coordinate := 349
            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

            square_top_x_coordinate := 131
            square_top_y_coordinate := 154
            square_bottom_x_coordinate := 146
            square_bottom_y_coordinate := 168

            mouse_click_right(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            ; random sleep_interval , 4000, 4300
            ; sleep sleep_interval

            square_top_x_coordinate := 42
            square_top_y_coordinate := 263
            square_bottom_x_coordinate := 177
            square_bottom_y_coordinate := 264
            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            random sleep_interval , 4000, 4300
            sleep sleep_interval
            ; mouse_click_right(detected_coordinates[1],detected_coordinates[2],detected_coordinates[3],detected_coordinates[4],delay_min,delay_max)
            ;  klkni na vrata
            square_top_x_coordinate := 484
            square_top_y_coordinate := 47
            square_bottom_x_coordinate := 495
            square_bottom_y_coordinate := 56

            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
        }

    }
    else{
        MsgBox % "RuneLite is not activated. Please click on RuneLite window." 
    }
    stop_script := False
    MsgBox % "skripta gotova." 

}

withdraw_pizza_cheese_making(){
    WinGetTitle, Title, A
    WinGet, OutputVar_id, ID , A
    WinGet, OutputVar_pid, PID , A
    WinGet, OutputVar_exe, ProcessName , A
    delay_min := 300
    delay_max := 500
    if(OutputVar_exe == "RuneLite.exe"){
        repeat_number := 185
        loop %repeat_number% {

            square_top_x_coordinate := 286
            square_top_y_coordinate := 182
            square_bottom_x_coordinate := 300
            square_bottom_y_coordinate := 199

            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
            random sleep_interval , 1100, 1300
            sleep sleep_interval

            square_top_x_coordinate := 438
            square_top_y_coordinate := 329
            square_bottom_x_coordinate := 456
            square_bottom_y_coordinate := 349
            mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

            pizza_image := A_ScriptDir . "\images\incomplete_pizza.png"

            ; x1 := 867
            ; y1 := 289
            ; x2 := 938
            ; y2 := 351

            x1 := 69
            y1 := 141
            x2 := 111
            y2 := 181
            detected_coordinates := detect_image_on_region(pizza_image,x1,y1,x2,y2)
            rez := True
            for index, value in detected_coordinates {
                ; MsgBox % index ": value: " value
                if (value == 0) {
                    rez := False
                }
            }
            if (rez){
                mouse_click_right(detected_coordinates[1],detected_coordinates[2],detected_coordinates[3],detected_coordinates[4],delay_min,delay_max)
                square_top_x_coordinate := 10
                square_top_y_coordinate := 230
                square_bottom_x_coordinate := 199
                square_bottom_y_coordinate := 236

                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

                square_top_x_coordinate := 320
                square_top_y_coordinate := 111
                square_bottom_x_coordinate := 342
                square_bottom_y_coordinate := 134

                mouse_click_right(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
                ; square_top_x_coordinate := 369
                ; square_top_y_coordinate := 185
                ; square_bottom_x_coordinate := 240
                ; square_bottom_y_coordinate := 191
                square_top_x_coordinate := 271
                square_top_y_coordinate := 191
                square_bottom_x_coordinate := 324
                square_bottom_y_coordinate := 193

                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

                square_top_x_coordinate := 486
                square_top_y_coordinate := 45
                square_bottom_x_coordinate := 497
                square_bottom_y_coordinate := 55
                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

                square_top_x_coordinate := 571
                square_top_y_coordinate := 247
                square_bottom_x_coordinate := 593
                square_bottom_y_coordinate := 261
                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

                square_top_x_coordinate := 659
                square_top_y_coordinate := 358
                square_bottom_x_coordinate := 674
                square_bottom_y_coordinate := 367
                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
                square_top_x_coordinate := 216
                square_top_y_coordinate := 421
                square_bottom_x_coordinate := 305
                square_bottom_y_coordinate := 485
                ; MsgBox % "ovdje stane: " 
                random sleep_interval , 500, 700
                sleep sleep_interval
                mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
                random sleep_interval , 15000, 20000
                sleep sleep_interval

                ; square_top_x_coordinate := 286
                ; square_top_y_coordinate := 182
                ; square_bottom_x_coordinate := 300
                ; square_bottom_y_coordinate := 199

                ;         mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)
                ; random sleep_interval , 1100, 1300
                ; sleep sleep_interval

                ; square_top_x_coordinate := 438
                ; square_top_y_coordinate := 329
                ; square_bottom_x_coordinate := 456
                ; square_bottom_y_coordinate := 349
                ;         mouse_click(square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate,delay_min,delay_max)

                ;         x1 := 5
                ; y1 := 221
                ; x2 := 209
                ; y2 := 244
                ; square_top_x_coordinate := 6
                ; square_top_y_coordinate := 213
                ; square_bottom_x_coordinate := 207
                ; square_bottom_y_coordinate := 249
                ;         image := A_ScriptDir . "\images\14_incomplete_pizzas.png"
                ;         detected_coordinates := detect_image_on_region(image,square_top_x_coordinate,square_top_y_coordinate,square_bottom_x_coordinate,square_bottom_y_coordinate)

            }
        }

    }
    else{
        MsgBox % "RuneLite is not activated. Please click on RuneLite window." 
    }
    stop_script := False
    MsgBox % "skripta gotova." 
}

login_completley(){
    select_world_mouse_click()
    sleep 50
    login_mouse_click()
}