if not A_IsAdmin
    Run *RunAs "%A_ScriptFullPath%" ; (A_AhkPath is usually optional if the script has the .ahk extension.) You would typically check first.

Process, Priority,, High 

#SingleInstance, Force
SendMode Event
; SendMode Input
SetWorkingDir, %A_ScriptDir%

#include %A_ScriptDir%\Gdip_All.ahk
; #include %A_ScriptDir%\GDIpHelper.ahk
#include %A_ScriptDir%\debug.ahk
#include %A_ScriptDir%\Vis2-master\lib\Vis2.ahk
#include %A_ScriptDir%\FindText.ahk
