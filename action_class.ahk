detect_image_on_region_and_click(image_dest,x1,y1,x2,y2){
    detected_coordinates := detect_image_on_region(image_dest,x1,y1,x2,y2)
    rez := True
    for index, value in detected_coordinates {
        ; MsgBox % index ": value: " value
        if (value == 0) {
            rez := False
        }
    }
    if (rez){
        mouse_click(detected_coordinates[1],detected_coordinates[2],detected_coordinates[3],detected_coordinates[4])
    }
    else{
        MsgBox % "Couldn't make a click because image is not detected" 
    }
}

detect_text_on_region_and_click(Text,x1,y1,x2,y2){
    ; Text:="|<new_user>*144$89.XlzzzzzwSDzzzzz3XzzzzzswTzzzzy77zzzzzlszzzzzw2DzzzzzXlzzzzzs4Tzzzzz7Xzzzzzk8zzzzzyD7zzzzzW1w3lwTwSDUDUS143lXXszswSDyAQ487777lzlsszsssQECCCBXzXls3lllskQ8w07z7XvXV7Xlks3s0DyD7z70T7XVlzkETyADyCDyD73lzllzy0S0yDwST7k7rrzy0s3y0sw"

    ; x11 := 365
    ; y11 := 461
    ; x22 := 556
    ; y22 := 499
    WinGetPos, absX, absY,absWidth ,absHeight , A ; "A" to get the active window's pos.

    rel_x1 := absX + x11
    rel_y1 := absY + y11
    rel_x2 := absX + x22
    rel_y2 := absY + y22
    if (ok:=FindText(rel_x1, rel_y1, rel_x2, rel_y2, 0, 0, Text))
        ; if (ok:=FindText(177-150000, 85-150000, 177+150000, 85+150000, 0, 0, Text))
    {
        x1 := ok.1.1
        y1 := ok.1.2
        w := ok.1.3
        h := ok.1.4
        x2 := x1 + w
        y2 := y1 + h
        mouse_click(x1,y1,x2,y2)
        return 1
    }
    else {
        return 0
    }

}

detect_image_on_active_window(image_dest){
    ; image_dest := "C:\Users\spolj\OneDrive\Documents\runscape_bots\images\test.png"
    overlay()
    found_x1 := 0
    found_y1 := 0
    found_x2 := 0
    found_x2 := 0

    WinGetPos, absX, absY,absWidth ,absHeight , A ; "A" to get the active window's pos.

    rel_x1 := absX
    rel_y1 := absY 
    rel_x2 := absX + absWidth
    rel_y2 := absY + absHeight

    gui,add,picture,hwndmypic,%image_dest%
    controlgetpos,,,pic_width,pic_height,,ahk_id %mypic%

    ImageSearch, FoundX, FoundY, rel_x1,rel_y1, rel_x2, rel_y2, *30 %image_dest%
    if (ErrorLevel = 2){
        MsgBox Could not conduct the image search.
    }
    else if (ErrorLevel = 1){
        MsgBox image could not be found on the screen.
    }
    else {
        ; MsgBox The icon was found at %FoundX%x%FoundY%.
        rec_x := FoundX + absX
        rec_y := FoundY + absY
        rec_x2 := FoundX + absX + pic_width
        rec_y2 := FoundY + absY + pic_height
        overlay(rec_x,rec_y,rec_x2,rec_y2)
        found_x1 := FoundX
        found_y1 := FoundY
        found_x2 := FoundX + pic_width
        found_y2 := FoundY + pic_height
    }
    return_array := []
    return_array.Push(found_x1)
    return_array.Push(found_y1)
    return_array.Push(found_x2)
    return_array.Push(found_y2)

    return return_array
}
detect_image_on_region(image_dest,x1,y1,x2,y2){
    ; image_dest := "C:\Users\spolj\OneDrive\Documents\runscape_bots\images\test.png"
    overlay()

    found_x1 := 0
    found_y1 := 0
    found_x2 := 0
    found_x2 := 0

    gui,add,picture,hwndmypic,%image_dest%
    controlgetpos,,,pic_width,pic_height,,ahk_id %mypic%

    WinGetPos, absX, absY,absWidth ,absHeight , A ; "A" to get the active window's pos.

    ; rel_x1 := absX
    ; rel_y1 := absY 
    ; rel_x2 := absX + absWidth
    ; rel_y2 := absY + absHeight

    ; ImageSearch, FoundX, FoundY, rel_x1,rel_y1, rel_x2, rel_y2, *30 %image_dest%
    ImageSearch, FoundX, FoundY, x1,y1,x2,y2, *30 %image_dest%
    if (ErrorLevel = 2){
        MsgBox Could not conduct the image search.
    }
    else if (ErrorLevel = 1){
        MsgBox image could not be found on the screen.
    }
    else {
        ; MsgBox The icon was found at %FoundX%x%FoundY%.
        rec_x := FoundX + absX
        rec_y := FoundY + absY
        rec_x2 := FoundX + absX + pic_width
        rec_y2 := FoundY + absY + pic_height
        overlay(rec_x,rec_y,rec_x2,rec_y2)
        found_x1 := FoundX
        found_y1 := FoundY
        found_x2 := FoundX + pic_width
        found_y2 := FoundY + pic_height
    }
    return_array := []
    return_array.Push(found_x1)
    return_array.Push(found_y1)
    return_array.Push(found_x2)
    return_array.Push(found_y2)

    return return_array
}

mouse_click(x1,y1,x2,y2,delay_min := 300,delay_max := 500,repeat := 1,c_speed := "random"){
    ; draw_rectangle(x1,y1,x2,y2)
    loop %repeat%{

        WinGetPos, absX, absY, , , A ; "A" to get the active window's pos.

        rec_width := x2 - x1
        rec_height := y2 - y1
        rec_x := absX + x1
        rec_y := absY + y1
        rec_x2 := absX + x2
        rec_y2 := absY + y2
        overlay(rec_x,rec_y,rec_x2,rec_y2)

        ; CoordMode, Mouse, Relative
        MouseGetPos, OriginX, OriginY
        if (OriginX >= x1 and OriginX <= x2 and OriginY >= y1 and OriginY <= y2){

            random, mouse_movement, 1, 30
            if (mouse_movement <= 10){
                random, xpos, x1, x2
                random, ypos, y1, y2
            }
            else if (mouse_movement <= 20 and mouse_movement > 10){
                random, xpos, x1, x2
                ypos := OriginY
            }
            else if (mouse_movement <= 30 and mouse_movement > 20){
                random, ypos, y1, y2
                xpos := OriginX
            }
            else {
                random, xpos, x1, x2
                random, ypos, y1, y2
            }
        }
        else{
            random, xpos, x1, x2
            random, ypos, y1, y2
        }
        ; MsgBox % "c_speed: " c_speed
        if (c_speed == "slowest"){
            ; speed := 100
            Random, speed, 40, 100
        }
        else if (c_speed == "random"){
            Random, speed, 0, 20
        }
        else {
            ; speed := 80
            speed := 10
        }
        ; Random, speed, 50, 100
        ; Random, speed, 0, 100
        ; speed := 100
        ; speed := 0
        MouseClick, Left , xpos, ypos,, speed ; WORKS
        overlay()
        ; MouseClick, Left , xpos, ypos,, speed ; WORKS
        ; Gui, XPT99:Destroy
        ; sleep 50
        ; MouseClick, Left , xpos, ypos,, speed ; WORKS
        ; MouseClick, Left , xpos, ypos,, 0 ; WORKS
        ; MouseClick, Left , 114, 769, 1, 50 ; WORKS
        ; MouseClick, Left , 114, 114, 1, 50,, R ; WORKS
        random, random_interval, delay_min,delay_max
        sleep, random_interval
        ; Gui, XPT99:Destroy
    }

}
mouse_click_right(x1,y1,x2,y2,delay_min,delay_max,repeat := 1,c_speed := "random"){
    ; draw_rectangle(x1,y1,x2,y2)
    loop %repeat%{

        WinGetPos, absX, absY, , , A ; "A" to get the active window's pos.

        rec_width := x2 - x1
        rec_height := y2 - y1
        rec_x := absX + x1
        rec_y := absY + y1
        rec_x2 := absX + x2
        rec_y2 := absY + y2
        overlay(rec_x,rec_y,rec_x2,rec_y2)

        ; CoordMode, Mouse, Relative
        MouseGetPos, OriginX, OriginY
        if (OriginX >= x1 and OriginX <= x2 and OriginY >= y1 and OriginY <= y2){

            random, mouse_movement, 1, 30
            if (mouse_movement <= 10){
                random, xpos, x1, x2
                random, ypos, y1, y2
            }
            else if (mouse_movement <= 20 and mouse_movement > 10){
                random, xpos, x1, x2
                ypos := OriginY
            }
            else if (mouse_movement <= 30 and mouse_movement > 20){
                random, ypos, y1, y2
                xpos := OriginX
            }
            else {
                random, xpos, x1, x2
                random, ypos, y1, y2
            }
        }
        else{
            random, xpos, x1, x2
            random, ypos, y1, y2
        }
        ; MsgBox % "c_speed: " c_speed
        if (c_speed == "slowest"){
            ; speed := 100
            Random, speed, 40, 100
        }
        else if (c_speed == "random"){
            Random, speed, 0, 20
        }
        else {
            ; speed := 80
            speed := 10
        }
        ; Random, speed, 50, 100
        ; Random, speed, 0, 100
        ; speed := 100
        ; speed := 0
        MouseClick, Right , xpos, ypos,, speed ; WORKS
        overlay()
        ; MouseClick, Left , xpos, ypos,, speed ; WORKS
        ; Gui, XPT99:Destroy
        ; sleep 50
        ; MouseClick, Left , xpos, ypos,, speed ; WORKS
        ; MouseClick, Left , xpos, ypos,, 0 ; WORKS
        ; MouseClick, Left , 114, 769, 1, 50 ; WORKS
        ; MouseClick, Left , 114, 114, 1, 50,, R ; WORKS
        random, random_interval, delay_min,delay_max
        sleep, random_interval
        ; Gui, XPT99:Destroy
    }

}
; mouse_click(x1,y1,x2,y2,delay_min,delay_max,repeat := 1,c_speed := "Fast"){
;     ; draw_rectangle(x1,y1,x2,y2)
;     loop %repeat%{

;         WinGetPos, absX, absY, , , A ; "A" to get the active window's pos.
;         ; absX := 0
;         ; absy := 0

;         ; Gui, XPT99:Destroy
;         Gui, XPT99:-DPIScale
;         Gui, XPT99:Margin , 0, 0
;         ; transform, current_status_icon, Deref, %current_status_icon% 
;         ; Gui, XPT99:Add, Picture, BackgroundTrans, %current_status_icon%
;         ; Gui, XPT99:Add, Picture, , %current_status_icon%
;         ; Gui, XPT99:Color, ECE9D8
;         Gui, XPT99:Color, ff0000
;         Gui, XPT99:+LastFound -Caption +AlwaysOnTop +ToolWindow -Border
;         transparency := 66
;         Winset, Transparent, %transparency%
;         ; Winset, TransColor, %transparency%

;         ; rec_width := x2 - x1
;         ; rec_height := y2 - y1
;         ; rec_x := x1
;         ; rec_y := y1
;         rec_width := x2 - x1
;         rec_height := y2 - y1
;         rec_x := absX + x1
;         rec_y := absY + y1
;         ; MsgBox % "x2: " x2
;         ; MsgBox % "y2: " y2
;         ; MsgBox % "x1: " x1
;         ; MsgBox % "y1: " y1
;         ; MsgBox % "rec_width: " rec_width
;         ; MsgBox % "rec_height: " rec_height
;         ; MsgBox % "rec_x: " rec_x
;         ; MsgBox % "rec_y: " rec_y
;         Gui, XPT99:Show, w%rec_width% h%rec_height% x%rec_x% y%rec_y% NoActivate
;         ; Gui, XPT99:Show, w500 h500 xCenter yCenter NoActivate

;         CoordMode, Mouse, Relative
;         MouseGetPos, OriginX, OriginY
;         random, mouse_movement, 1, 30
;         if (mouse_movement <= 10){
;             random, xpos, x1, x2
;             random, ypos, y1, y2
;         }
;         else if (mouse_movement <= 20 and mouse_movement > 10){
;             random, xpos, x1, x2
;             ypos := OriginY
;         }
;         else if (mouse_movement <= 30 and mouse_movement > 20){
;             random, ypos, y1, y2
;             xpos := OriginX
;         }
;         else {
;             random, xpos, x1, x2
;             random, ypos, y1, y2
;         }

;         if (c_speed == "slowest"){
;             Random, speed, 20, 80
;         }
;         else if (c_speed == "random"){
;             Random, speed, 0, 20
;         }
;         else {
;             speed := 0
;         }
;         ; Random, speed, 50, 100
;         ; Random, speed, 0, 100
;         ; speed := 100
;         ; speed := 0
;         MouseClick, Left , xpos, ypos,, speed ; WORKS
;         Gui, XPT99:Destroy
;         sleep 50
;         MouseClick, Left , xpos, ypos,, speed ; WORKS
;         ; MouseClick, Left , xpos, ypos,, 0 ; WORKS
;         ; MouseClick, Left , 114, 769, 1, 50 ; WORKS
;         ; MouseClick, Left , 114, 114, 1, 50,, R ; WORKS
;         random, random_interval, delay_min,delay_max
;         sleep, random_interval
;         ; Gui, XPT99:Destroy
;     }

; }
