overlay(x1 := 0, y1 := 0, x2 := 0, y2 := 0) {
    static HWND := makeGui()
    if !(x1 || y1 || x2 || y2)
        Gui %HWND%: Hide
    else
        Gui %HWND%: Show, % Format("x{} y{} w{} h{} NoActivate", x1, y1, x2 - x1, y2 - y1)
}
overlay2(x1 := 0, y1 := 0, w := 0, h := 0) {
    static HWND := makeGui()
    if !(x1 || y1 || x2 || y2)
        Gui %HWND%: Hide
    else
        Gui %HWND%: Show, % Format("x{} y{} w{} h{} NoActivate", x1, y1, w, h)
}

makeGui() {
    Gui, -DPIScale
    Gui, Margin , 0, 0
    Gui New, +AlwaysOnTop -Caption +ToolWindow +HWNDhwnd -Border +LastFound +E0x20
    Gui Color, Red
    Gui Show, NoActivate
    WinSet Trans, 50, % "ahk_id" hwnd
    Gui Show, Hide NoActivate

    return hwnd
}